AWPGen - The Astronomical Wallpaper Generator.
By Dale Maggee

********************************************************************************
DEPRECATION NOTICE
********************************************************************************
After well over a decade, I am deprecating awpgen. 
 It will recieve no future updates.

But: every cloud has a silver lining! The reason I'm deprecating awpgen is...

AWPGEN2. See: https://gitlab.com/antisol/awpgen2

Awpgen2 is a rewrite in python, built to be much cleaner, more extensible,
 and most of all more efficient. Here are some of the nicest features:

- Uses an optimised tiff format and a new image library to handle 
	insanely huge deepfield images efficiently in a fraction of a second 
	without using huge amounts of memory or disk space.

- Restores about 20 years of apod imagery by using the full apod index
	(at some point their regular index page changed to only show images
	from >=2015)

- New plugin architecture! And new modes! e.g the "AntiSol" plugin lets you
   see my personal nerdy photography (from https://antisol.org/gallery) 
   mixed in with APOD imagery! Maybe I'll add more plugins one day

- Multimonitor support

If you wish to continue using this version, pass --stubborn on the CLI

Thanks for using awpgen
********************************************************************************

Space is big. Really big. You just won't believe how vastly, hugely, mindbogglingly big it is... Unless you have the Astronomical Wallpapaer Generator.

AWPGen is a program for geeks to have their wallpaper change randomly, with enough images available that you're not going to get bored any time soon.

AWPGen is a command-line tool for generating interesting astronomy-themed images randomly, ostensibly for use in a cron job overwriting your desktop wallpaper at a regular interval.

awpgen has 2 modes of operation:

1. Hubble Mode

Hubble Mode uses the Hubble Ultra Deep Field image (or, in fact, any large image), which is absurdly large in size. So large, in fact, that no standard monitor can display the entire image properly: The scaled down image you see on your puny computer monitor misses all the fine detail in this image, and it's the amazing detail, and the huge number of tiny pinpoints of light, each of which is a galaxy, which makes this picture so cool. Hubble Mode takes a random section of this image which is the size of your screen at 1:1 resolution, allowing you to be almost-properly astounded by the sheer vastness of the observable universe.

You can place one or more deep field images in ~/.awpgen/ with a filename starting with 'deepfield', and by default awpgen will randomly select one of these images when hubble mode is invoked. 

You can use the '-l' and '-g' command-line parameters to do this automatically - the '-l' switch lists known deep field images, and '-g idx' allows you to download these images (where idx is a number from the list)

If no deep field images are found, awpgen will try to download one (The 2014 Hubble Ultra Deep Field, relatively small file).

You can also use the -d command-line parameter to specify a particular deep field image. Most image formats are supported (anything supported by ImageMagick / RMagick).


2. APOD Mode

I've discovered that even random sections of huge Deep-field images can become boring after a few years. So, in order to vary things up even more, we have APOD Mode.
APOD Mode uses NASA's "Astronomy Picture Of The Day" website to get a random astronomy image. Since we're looking for pretty wallpapers, we apply certain restrictions on what images we retrieve from the APOD Site:

 - we have a user-modifyable black list which allows us to exclude any image we want.
 	awpgen handles "exclusions files" - any file matching ~/.awpgen/exclusions.* will be processed. 
	Exclusions files contain pages to ignore, one per line. lines starting with "#" are comments.
	Anything awpgen excludes automatically goes into ~/.awpgen/exclusions.auto
	Anything excluded with --suckage goes into ~/.awpgen/exclusions.user
 
 - we have a "--suckage" command line parameter which nicely adds the most recently downloaded image to the blacklist, and generates another wallpaper.
 
 - we only retrieve jpeg, png, and tif images, since these seem to be NASA's preferred formats for big, pretty images on the APOD site. The APOD site does use a bunch of other formats ranging from gifs to youtube videos, but on the whole it looks like these aren't usually worth putting on your wallpaper - they're charts and low-res pics which wouldn't scale nicely.
 
 - we only accept images that are at least a certain size. This prevents us from seeing horrible upscaled blurs when the APOD image is small. By default, this size is 640x480, but this can be configured.
 
 - we're clever: When an image fails these tests, it's automagically blacklisted, so that we don't waste time or bandwidth trying to use it again.
 	(~/.awpgen/exclusions.auto)
 
The retrieved APOD image will be scaled to fill your screen resolution. By default, this means cropping in most cases (few images are the same Aspect Ratio as your screen). This behaviour can be changed, with the image scaled to fit. There's also an option to disable scaling altogether.

We cache the APOD index file, only retrieving it once per day.

APOD mode will only make a certain number of attempts to get an APOD image before falling back to hubble mode, so if your intarwebs has gone away your wallpaper will still change (to a hubble-mode image)

Images available:

The APOD Website has over 6000 beautiful images available - this grows daily, hence the "of the day" part in the name.

The Hubble Deep Field Image contains 384440000 pixels (38.4MP) - this is 20 times the area of your screen if you run at full HD resolution (1920x1080), and 50 times your screen area if you run at 1024x768.

awpgen now has a list of multiple deep field images, ranging from 'big' to 'insanely huge'. use '-l' and '-g' to list/retrieve them.

Command line options:

run 'awpgen -h'.

CONFIGURIFICATION

awpgen is configurable. It will attempt to load config options from ~/.awpgen/awpgen.cfg

every time it runs, awpgen will save all the current options to ~/.awpgen/awpgen_last.cfg, giving you a good example of a config file and the available options. All of these are optional and have default values.

Note that if you copy or rename awpgen_last.cfg over to awpgen.cfg, you'll probably want to delete or comment out the 'mode' line, since the 'mode' line in awpgen_last will indicate whether APOD or hubble mode was chosen, and you probably want the mode to be chosen randomly.

See 'awpgen --help' for a list of command-line options


APOD Mode Options (not yet implemented):
	-1	--one-to-one	Output APOD images at original size, assuming that your desktop environment will handle it properly.
	-n	--no-fallback	Program will exit with nonzero status rather than fallback from APOD to Hubble mode.
	
	-S	--sizecheck-off	Disable size check - allow APOD images of any size. can be achieved with '--min 1x1'
	-T	--typecheck-off	Disable type check - awpgen will attempt to load any image it finds, regardless of type.


REQUIREMENTS:

* nokogiri
	sudo gem install nokogiri
* rmagick
	sudo gem install rmagick
	


	
Using AWPGen:

1. Run awpgen to generate a wallpaper image, e.g:
	awpgen ~/awpgen_img.jpg
  This should create an image file in your home directory called 'awpgen_img.jpg'.
  
2. In your desktop environment (xfce, gnome, kde, 'other'), choose the image you just generated as your wallpaper.

3. You can now run awpgen again to overwrite the background image:
  Your desktop environment may or may not automatically detect that the image has been updated and redraw your wallpaper.
  XFCE falls into the 'may not' category, so if you use XFCE so you should add '-x' to the command line and awpgen will try to refresh the xfce wallpaper for you (not always successfully - xfce has a habit of changing how this is done. Please whinge to the xfce authors if this causes you distress).

	awpgen -x ~/awpgen_img.jpg

4. You can put awpgen in your contab, e.g to make your wallpaper update every hour. Type 'crontab -e', and add something like the following line to your crontab:

0 * * * * /home/antisol/awpgen/awpgen -r 1280x800 /home/antisol/awpgen_img.jpg

OR:

0 * * * * /home/antisol/awpgen/awpgen -c ~/.awpgen/wallpaper.cfg

This will overwrite your image with a new one every hour, on the hour.

Note that it's important to specify your resolution in the contab command line, since awpgen can't detect it properly when it's running as a cron job. To find your resolution, look at awpgen's output when you run it without '-r' (it should autodetect your resolution and tell you), or do 'xrandr' at your terminal, and look for 'current: W x H'. For awpgen you shouldn't have spaces when specifying the resolution, it's '1024x768' or '1920x1080', not '1024 x 768' or '1920 x 1080'.

Note #2: The XFCE hack doesn't seem to work from the crontab, but the 'refresh_xfce_wallpaper' script does. XFCE users will want to add something like the following commands to their crontab instead, to run the 'refresh wallpaper' script:

0 * * * * /home/antisol/awpgen/awpgen -xr 1920x1080 /home/antisol/awpgen_img.jpg && /home/antisol/awpgen/refresh_xfce_wallpaper

Note #3: APOD mode can download large (many megabyte) images! Be sensible: don't have -A in your crontab running every 5 minutes for any length of time! make sure you have enough bandwidth for whatever you set up. You may want to use -R to change how often APOD mode is chosen. By default, awpgen will fall back to hubble mode if it can't get an apod image, so if your connection drops out, your wallpaper should still change (to a deepfield image).

Also, you probably shouldn't set your crontab job to run on the hour, i.e the zero in the first column should be something else - we don't want to DDOS the NASA website by all downloading images at x:00. Be a good netizen and set it to some other number instead so that the load from awpgen is spread out.

Files

awpgen creates a configuration directory in the user's home directory - ~/.awpgen/

In this directory, you may or may not find several files which awpgen generates. Any of these files may be deleted and awpgen will continue to function (and regenerate them), but some contain user preferences (e.g: exclusions.user):

Filename			Description
--------------			------------------
apod.cache		Cache file containing the APOD index data. refreshed once per day, or if the file does not exist

apod_image.jpg	This is the full-size version of the last image retrieved from APOD.

apod.selection		Contains the url of the last selected APOD image, or '<hubble>' if hubble mode was last used.

deepfield.jpg		This is the Hubble Deep Field image used for Hubble Mode. It will be downloaded here automagically if it's needed but not found.

exclusions.auto		This is the list of APOD images (urls) which awpgen has automatically rejected, either because they are too small, were not parseable (i.e: no useable image), or were unsupported types (gif). The file includes comments which advise why each exclusion was added.

exclusions.user		This is APOD images excluded by the user passing '-s' to awpgen.

awpgen.cfg			Not automatically generated. awpgen will try to load this config file on startup

awpgen_last.cfg		A config file with the options used last time awpgen was run. Note that you may want to comment out the 'mode' and 'outfile' lines if you use this as your config file.

Exclusions files.

You can put exclusions files in either ~/.awpgen/ or /etc/awpgen/. exclusions files should have a name beginning with 'exclusions'. The app's convention is to use 'exclusions.description', but this is not enforced, you could equally well use something like 'exclusions_yourname.txt', as long as the filename begins with 'exclusions' and lives in one of these 2 directories, it will be loaded.

Exclusions files can have comments - any line beginning with a hash character '#' will be ignored.

Each line in an exclusions file is a new rule. Each rule is a plain text (not regex! too slow!) match which is applied to either the image title or the image url.

Note that url exclusions - anything which starts with 'ap' - are designed to be unique and are only applied once - they are removed from the ruleset after being applied to speed things up.

Title matches (anything which doesn't start with 'ap') can apply to many images, so specifying something like "Orion" or "M42" will omit multiple APOD images (probably really good ones! I *don't* recommend these as good rules!)

CAVEATS

When dealing with large images such as the andromeda 40k image, ImageMagick can use a fairly large amount (~3gb) of temporary disk space. If you don't have enough free space, awpgen will error.

TODO

* Support for APOD mirrors (to distribute server load, for when APOD goes down)
* Support for other non-APOD image libraries / galleries, e.g: http://www.hq.nasa.gov/office/pao/History/alsj/

